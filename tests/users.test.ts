import { Db } from "../src/services/db.service";
import { SERVICE_TYPES } from "../src/di/types";
import container from "../src/di/inversify.config";
import { UserDoc } from "../src/schemas/user";

describe("Create User", () => {
  beforeAll(async () => {
    await container.get<Db>(SERVICE_TYPES.Db).init();
  });
  it("return the created user", async () => {
    let user = new UserDoc();
    expect(user.save).toThrowError();
  });
  afterAll(async()=>{
    await container.get<Db>(SERVICE_TYPES.Db).disconnect();
  })
});
  