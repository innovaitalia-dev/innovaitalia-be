import "reflect-metadata";
import {Container} from "inversify";
import {SERVICE_TYPES} from "./types";
import { Db } from "../services/db.service";
import { ConversationService } from "../services/conversation.service";
import { UserService } from "../services/user.service";
import { ParsingExtService } from "../services/parsing.ext.service";
import { AccessTrackerService } from "../services/accesstracker.service";

let container = new Container();

container.bind<Db>(SERVICE_TYPES.Db).to(Db).inSingletonScope();
container.bind<ConversationService>(SERVICE_TYPES.Conversation).to(ConversationService).inSingletonScope();
container.bind<UserService>(SERVICE_TYPES.User).to(UserService).inSingletonScope();
container.bind<ParsingExtService>(SERVICE_TYPES.ParsingExt).to(ParsingExtService).inSingletonScope();
container.bind<AccessTrackerService>(SERVICE_TYPES.AccessTracker).to(AccessTrackerService).inSingletonScope();


export default container;