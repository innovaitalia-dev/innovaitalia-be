import { Router } from "express";
import { CompanyDoc, SedeDoc, CompanyDocType } from "../../schemas/company";
import { CheckCompanyMiddleware } from "../../middleware/companyAuth";
import { createResponse, getAuthorizedCompany } from "../../utils/routes";
import container from "../../di/inversify.config";
import { AccessTrackerService } from "../../services/accesstracker.service";
import { SERVICE_TYPES } from "../../di/types";
const router = Router();

router.get('/', CheckCompanyMiddleware, (req, res, next) => {
    let company: CompanyDocType = getAuthorizedCompany(req);
    CompanyDoc.findById(company._id).exec().then(
        value => {
            if (!value) {
                let error = new Error("Company not found");
                res.statusCode = 404;
                next(error)
            } else
                res.json(createResponse(res, "Company found", value));
        }
    ).catch(error => {
        next(error);
    });
});

router.patch('/', CheckCompanyMiddleware, (req, res, next) => {
    let company: CompanyDocType = getAuthorizedCompany(req);
    CompanyDoc.update({_id: company._id}, {$set :
        req.body
    }).exec().then(result => {
        if (result.nModified != 0){
            CompanyDoc.findById(company._id).then(company => {
              res.json(createResponse(res, "Profile Modified", {
                company: company
              }))
            });
          } else {
            res.json(createResponse(res,"Profile Not Modified", {}));
          }
    })
})

router.get('/getSeat', CheckCompanyMiddleware, async (req, res, next) => {
        let company: CompanyDocType = getAuthorizedCompany(req);
        const seat = (await container.get<AccessTrackerService>(SERVICE_TYPES.AccessTracker).checkAvailableSeat(company._id));
        createResponse(res, "Total available seat for this area", {seat: seat});
    }
)


export default router;