import { Router } from "express";
import { ICompany, ISede, IArea } from "../../models/company";
import { Joi, celebrate, Segments } from "celebrate";
import { SchemaMap } from "joi";
import { CompanyDoc } from "../../schemas/company";
import bcrypt from "bcryptjs";
import { createResponse } from "../../utils/routes";
import config from "../../config";
import jwt from "jsonwebtoken";
const router = Router();

// const accountBodySchema: IAccount = {
//     nome: Joi.string().required(),
//     email: Joi.string().email().required(),
//     password: Joi.string().required()
// }
const accessiBodySchema: IArea = {
    nome: Joi.string().required(),
    posti: Joi.number().min(0).required(),
    //account: Joi.object().keys(accountBodySchema)
}
const sediBodySchema: ISede = {
    nome: Joi.string().required(),
    indirizzo: Joi.string().required(),
    telefono: Joi.string().required(),
    aree: Joi.array().items(Joi.object().keys(accessiBodySchema)),
}
const companyBodySchema: ICompany = {
    nome: Joi.string().required(),
    password: Joi.string().required(),
    codiceFiscale: Joi.string().required(),
    sedi: Joi.array().items(Joi.object().keys(sediBodySchema)).required()
}

const loginBodySchema = {
    codiceFiscale: Joi.string().required(),
    password: Joi.string().required()
}

router.post(
    '/signup',
    celebrate({
        [Segments.BODY]: Joi.object().keys(
            (companyBodySchema as unknown) as SchemaMap
        )
    }),
    (req, res, next) => {
        CompanyDoc.findOne({ codiceFiscale: req.body.codiceFiscale }).exec().then(value => {
            if (!value) {
                let pwd = req.body.password;
                bcrypt.hash(pwd, 10, (err, hash) => {
                    req.body.password = hash;
                    const company = new CompanyDoc(req.body);
                    company.save().then(value => {
                        res.status(201).json(createResponse(res, "Company Created", value));
                    }).catch(err => {
                        err.status = 400;
                        next(err);
                    })
                })
            } else {
                let error = new Error("Company exists");
                res.statusCode = 409;
                next(error);
            }
        }).catch(err => {
            next(err);
        })
    }
);

router.post(
    '/signin',
    celebrate({
        [Segments.BODY]: Joi.object().keys(loginBodySchema)
    }),
    (req, res, next) => {
        CompanyDoc.findOne({ codiceFiscale: req.body.codiceFiscale }).then(value => {
            if (!value) {
                let error = new Error("Authentication failed");
                res.statusCode = 401;
                next(error);
            } else {
                console.log(req.body.password);
                console.log(value.password);
                bcrypt.compare(req.body.password, value.password, function (err, result) {
                    if (err) next(err);
                    if (result) {
                        let token = jwt.sign({ codiceFiscale: value.codiceFiscale }, config.jwt.secret, {
                            expiresIn: config.jwt.exp
                        });
                        res.json(createResponse(res, "Authentication succeded", {
                            company: value,
                            jwt: token
                        }));
                    } else {
                        let error = new Error("Authentication failed");
                        res.statusCode = 401;
                        next(error);
                    }
                });
            }
        }).catch(error => {
            next(error);
        });
    }
)

export default router;

