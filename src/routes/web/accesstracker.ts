import { Router } from "express";
import { celebrate, Segments, Joi } from "celebrate";
import { CheckCompanyMiddleware } from "../../middleware/companyAuth";
import { SERVICE_TYPES } from "../../di/types";
import container from "../../di/inversify.config";
import { AccessTrackerService } from "../../services/accesstracker.service";
import { createResponse, createErrorResponse, getAuthorizedCompany } from "../../utils/routes";
import { CompanyDocType } from "../../schemas/company";

const router = Router();

const submitAccessSchema = {
    userId: Joi.string().required()
}

const submitGuestAccessSchema = {
    autocertificazione: Joi.array().items(Joi.object().keys({
        nome: Joi.string().required(),
        valore: Joi.string().required()
    })).required()
}

router.post(
    '/submit',
    CheckCompanyMiddleware,
    celebrate({
        [Segments.BODY]: Joi.object().keys(submitAccessSchema)
    }),
    async (req, res, next) => {
        let company: CompanyDocType = getAuthorizedCompany(req);
        container.get<AccessTrackerService>(SERVICE_TYPES.AccessTracker).submitUserAccess(req.body.userId,company).then(response => {
            res.statusCode = response.code;
            res.json(response);
        }).catch((error: any) => {
            next(error);
        });
    }
);

router.post(
    '/submitGuest',
    CheckCompanyMiddleware,
    celebrate({
        [Segments.BODY]: Joi.object().keys(submitGuestAccessSchema)
    }),
    async (req, res, next) => {
        res.json({response: "This service will be available soon"});
    }
)

router.get(
    '/getList',
    CheckCompanyMiddleware,
    async (req, res, next) => {
        let response = (await container.get<AccessTrackerService>(SERVICE_TYPES.AccessTracker).getListAccessForCompany(req.query.fromDate, req['company']._id));
        if(response != undefined) {
            createResponse(res, "Lista accessi", response);
        } else {
            createErrorResponse(res, "ERR_NO_ACCESS_FOUND", "Error");
        }
    }
)

export default router;