import { Router } from "express";
import compRouter from "./company";
import authRouter from "./auth";
import accessRouter from "./accesstracker";
const router = Router();
router.use('/company', compRouter);
router.use('/auth', authRouter);
router.use('/access', accessRouter);
export default router;