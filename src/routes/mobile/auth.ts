import bcrypt from "bcryptjs";
import { Router } from "express";
import jwt from "jsonwebtoken";
import config from "../../config";
import { celebrate, Segments, Joi } from "celebrate";

import { IUser } from "../../models/user";
import { createResponse } from "../../utils/routes";
import { SchemaMap } from "joi";
import { UserDoc } from "../../schemas/user";
const router = Router();
const registerBodySchema: IUser = {
  email: Joi.string().email(),
  firstName: Joi.string(),
  lastName: Joi.string(),
  birthDate: Joi.date(),
  password: Joi.string(),
  phoneNumber: Joi.string(),
  emergencyPhoneNumber:Joi.string(),
  openID: Joi.string(),
  sex: Joi.number(),
  personalNumber: Joi.string(),
  domicileCity: Joi.string(),
  domicileAddress: Joi.string(),
  residencyCity: Joi.string(),
  residencyAddress: Joi.string()
};

const loginBodySchema = {
  email: Joi.string().email(),
  password: Joi.string()
};

const loginBodyOpenId = {
  openID: Joi.string()
};
router.post(
  "/signup",
  celebrate({
    [Segments.BODY]: Joi.object().keys(
      (registerBodySchema as unknown) as SchemaMap
    )
  }),
  (req, res, next) => {
    UserDoc.findOne({ email: req.body.email })
      .exec()
      .then(value => {
        if (!value) {
          let pwd = req.body.password || "MyStupidPassword";
          bcrypt.hash(pwd, 10, function(err, hash) {
            req.body.password = hash;
            const c = new UserDoc(req.body);
            c.save()
              .then(value => {
                console.log(value);
                res.statusCode = 201;
                res
                  .status(201)
                  .json(createResponse(res, "User Created", value));
              })
              .catch(error => {
                error.status = 400;
                next(error);
              });
          });
        } else {
          let error = new Error("Mail exists");
          res.statusCode = 409;
          next(error);
        }
      })
      .catch(error => {
        next(error);
      });
  }
);

router.post(
  "/signin",
  celebrate({
    [Segments.BODY]: Joi.object().keys(loginBodySchema)
  }),
  (req, res, next) => {
    UserDoc.findOne({ email: req.body.email })
      .exec()
      .then(value => {
        if (!value) {
          let error = new Error("Authentication failed");
          res.statusCode = 401;
          next(error);
        } else {
          bcrypt.compare(req.body.password, value.password, function(
            err,
            result
          ) {
            if (err) next(err);
            if (result) {
              var token = jwt.sign({ email: value.email }, config.jwt.secret, {
                expiresIn: config.jwt.exp
              });
              res.json(
                createResponse(res, "Authentication succeded", {
                  user: value,
                  jwt: token
                })
              );
            } else {
              let error = new Error("Authentication failed");
              res.statusCode = 401;
              next(error);
            }
          });
        }
      })
      .catch(error => {
        next(error);
      });
  }
);

router.post(
  "/signin/openID",
  celebrate({
    [Segments.BODY]: Joi.object().keys(loginBodyOpenId)
  }),
  (req, res, next) => {
    UserDoc.findOne({ openID: req.body.openID })
      .exec()
      .then(value => {
        if (!value) {
          let error = new Error("Authentication failed");
          res.statusCode = 401;
          next(error);
        } else {
          var token = jwt.sign({ email: value.email }, config.jwt.secret, {
            expiresIn: config.jwt.exp
          });
          res.json(
            createResponse(res, "Authentication succeded", {
              user: value,
              jwt: token
            })
          );
        }
      })
      .catch(error => {
        next(error);
      });
  }
);
export default router;
