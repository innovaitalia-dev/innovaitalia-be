import { Router } from "express";
import { celebrate, Segments, Joi } from "celebrate";
import { CheckUserMiddleware } from "../../middleware/userAuth";
import container from "../../di/inversify.config";
import { ConversationService } from "../../services/conversation.service";
import { SERVICE_TYPES } from "../../di/types";
import { UserService } from "../../services/user.service";
import { UserDocType } from "../../schemas/user";
import { createResponse, getAuthorizedUser } from "../../utils/routes";
import { SnapshotDoc } from "../../schemas/snapshot";
import { ISnapshot } from "../../models/snapshot";
import moment = require("moment");
const router = Router();

router.post(
  "/message",
  CheckUserMiddleware,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      message: Joi.string().allow("")
    })
  }),
  async (req, res, next) => {
    let userMessage = req.body.message;
    let user: UserDocType = getAuthorizedUser(req);
    let genContext = {};
    let convService = container.get<ConversationService>(
      SERVICE_TYPES.Conversation
    );
    console.log("Session exists", convService.sessionExists(user.id));
    if (!convService.sessionExists(user.id))
      genContext = await container
        .get<UserService>(SERVICE_TYPES.User)
        .generateContextForUser(user);
    console.log("GenContext",genContext);
    let { context, response } = await convService.message(
      user.id,
      genContext,
      userMessage
    );
    if(context.endConversation){
      let snapBody:ISnapshot={
        timestamp:moment().toDate(),
        userId:user.id,
        data:context.result
      }
      let snap=new SnapshotDoc(snapBody);
      await snap.save();
      user.lastUpdate=moment().toDate(),
      user.lastWeeklyUpdate=moment().toDate()
      await user.save();
      
    }
    res.json(createResponse(res, "Message generated", { messages: response,context:context }));

  }
);
export default router;
