import {Router} from 'express';
import { CheckUserMiddleware } from '../../middleware/userAuth';
import { CompanyDoc } from '../../schemas/company';
import { createResponse } from '../../utils/routes';
const router = Router();
router.get('/', CheckUserMiddleware, (req, res, next) => {
    CompanyDoc.find().exec().then(
        companies => {
            if(!companies){
                let error = Error("No company found");
                res.statusCode = 404;
                next(error);
            } else {
                res.json(createResponse(res, "List of companies", companies));
            }
        }
    ).catch(
        err => {next(err)}
    )
});

router.get('/:id', CheckUserMiddleware, (req, res, next) => {
    let id = req.params.id;
    CompanyDoc.findById(id).exec().then(
        company => {
            if (!company){
                let error = Error("Company not found");
                res.statusCode = 404;
                next(error);
            } else {
                res.json(createResponse(res, "Company found", company));
            }
        }
    ).catch(
        err => {next(err)}
    )
})
export default router;