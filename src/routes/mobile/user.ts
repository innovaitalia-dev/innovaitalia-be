import { Router } from "express";
import { celebrate, Segments, Joi } from "celebrate";
import { CheckUserMiddleware } from "../../middleware/userAuth";
import { createResponse, createErrorResponse, getAuthorizedUser } from "../../utils/routes";
import jwt from "jsonwebtoken";
import config from "../../config";
import { UserDoc, UserDocType } from "../../schemas/user";
import { SnapshotDoc } from "../../schemas/snapshot";
import { create } from "domain";

const router = Router();

router.get(
  "/",
  CheckUserMiddleware,
  celebrate({
    [Segments.BODY]: Joi.object().keys({
      message: Joi.string()
    })
  }),
  async (req, res, next) => {
    let user: UserDocType = getAuthorizedUser(req);
    UserDoc.findOne({ _id: user._id })
      .exec()
      .then(value => {
        if (!value) {
          let error = new Error("Not Found");
          res.statusCode = 404;
          next(error);
        } else {
          res.json(
            createResponse(res, "Profile found", {
              user: value
            })
          );
        }
      })
      .catch(error => {
        next(error);
      });
  }
);

router.patch(
  '/',
  CheckUserMiddleware,
  (req, res, next) => {
    let user: UserDocType = getAuthorizedUser(req);
    UserDoc.update({_id: user._id}, {$set: req.body}).exec().then(result => {
      if (result.nModified != 0){
        UserDoc.findById(user._id).then(user => {
          res.json(createResponse(res, "Profile Modified", {
            user: user
          }))
        });
      } else {
        res.json(createResponse(res,"Profile Not Modified", {}));
      }
    })
  }
)

router.get('/snapshot', CheckUserMiddleware, (req, res, next) => {
  // TODO: Add date treshold
  let user: UserDocType = getAuthorizedUser(req);
  SnapshotDoc.findOne({ userId: user._id }).exec().then(value => {
    if (!value) {
      let error = new Error("Not Found");
      res.statusCode = 404;
      next(error);
    } else {
      res.json(
        createResponse(res, "Profile found", {
          snapshot: value
        })
      );
    }
  }).catch(error => {
    next(error);
  });
});

router.delete('/', CheckUserMiddleware, (req, res, next) => {
  let user: UserDocType = getAuthorizedUser(req);
  UserDoc.deleteOne({ _id: user._id }).exec().then(async (deletedUserResult: any) => {
    if(deletedUserResult.deletedCount == 0) {
      return createErrorResponse(res, "ERR_CANNOT_CANCEL_USER", "Identifier not corresponding with no user on DB", 404);
    }
    //TODO: gestione del ritorno
    let numSnapshotsDeleted: number;
    await SnapshotDoc.deleteMany({userId: user._id}).exec().then((deletedSnapsResult: any) => {
      numSnapshotsDeleted = deletedSnapsResult.deletedCount;
    }).catch(error => {
      next(error);
    });
    createResponse(res, "User successfully deleted", {snapshotsDeleted: numSnapshotsDeleted});
  }).catch(error => {
    next(error);
  });
});


export default router;

