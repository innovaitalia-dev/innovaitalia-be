import { Router } from "express";
import authRouter from "./auth";
import convRouter from "./message";
import userRouter from "./user";
import compRouter from "./company";
const router = Router();
router.use("/auth", authRouter);
router.use("/conv", convRouter);
router.use("/user", userRouter);
router.use("/company", compRouter);
export default router;