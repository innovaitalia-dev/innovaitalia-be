import { Router } from "express";
import mobileRouter from "../routes/mobile/index";
import webRouter from "../routes/web/index";


export default () => {
  const appRouter = Router();
  appRouter.use("/mobile", mobileRouter);
  appRouter.use("/web", webRouter);
  return appRouter;
};
