import passport from "passport";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import config from "../config";
import { CompanyDoc } from "../schemas/company";
import { Company } from "../models/company";

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwt.secret
};

passport.use(
  "companyStrategy",
  new JwtStrategy(opts, function(jwt_payload, done) {
    CompanyDoc.findOne({ codiceFiscale: jwt_payload.codiceFiscale }, function(err, company: Company) {
      if (err) {
        return done(err, false);
      }
      if (company) {
        return done(null, company);
      } else {
        return done(null, false);
      }
    });
  })
);
export const CheckCompanyMiddleware = passport.authenticate("companyStrategy", {
  session: false
});
