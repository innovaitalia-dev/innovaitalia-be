import { injectable } from "inversify";
import { UserDoc } from "../schemas/user";
import { IUser } from "../models/user";
import { EntryType, IAccessTracker, AccessTracker } from "../models/accesstracker";
import container from "../di/inversify.config";
import { ParsingExtService } from "./parsing.ext.service";
import { SERVICE_TYPES } from "../di/types";
import moment from "moment";
import { AccessTrackerDoc, AccessTrackerDocType } from "../schemas/accesstracker";
import { UserService } from "./user.service";
import { createErrorResponseBody, createResponseBody } from "../utils/routes";
import { ParsedDataModel } from "../models/parserrequest";
import { CompanyDoc } from "../schemas/company";
import { Company } from "../models/company";

@injectable()
export class AccessTrackerService {

    constructor() {};

    async getTodayUserAccess(userId: string) : Promise<AccessTrackerDocType[]> {
        let todayAccesses = (await AccessTrackerDoc.find({ userId: userId}).exec()).filter(
          access => {
            return (moment(access.timestamp).isSame(moment().startOf("day"), "d"));
          }
        );
        return todayAccesses;
    }

    securityLastEntryDifferentAreaCheck(lastAccess: AccessTrackerDocType, areaId: string) : boolean {
        return (lastAccess.areaId.toString() != areaId && lastAccess.type == EntryType.ENTER)
    }

    getAccessType(todayAccesses: AccessTrackerDocType[]) : EntryType {
        return (todayAccesses[todayAccesses.length-1].type == EntryType.EXIT) ? EntryType.ENTER : EntryType.EXIT;
    }

    async checkAvailableSeat(companyId: string) : Promise<number>{
        let totalAvailableSeat: number;
        let areaId: string; 
        await CompanyDoc.findOne({_id: companyId}).exec().then((company: Company) => {
            totalAvailableSeat = company.sedi[0].aree[0].posti;
            areaId = company.sedi[0].aree[0]._id;
        });
        await AccessTrackerDoc.find({areaId: areaId}).exec().then((accesses: AccessTracker[]) =>  {
            accesses.filter(access => {
                return (moment(access.timestamp).isSame(moment().startOf("day"), "d"));
            }).forEach(access =>{
                if(access.type == EntryType.ENTER) {
                    totalAvailableSeat = totalAvailableSeat -1;
                } else {
                    totalAvailableSeat = totalAvailableSeat +1;
                }
            });
        });
        return totalAvailableSeat;
    }

    checkUserHealthData(data: any) : boolean{
        let response: boolean = true;
        data.body.forEach((element: ParsedDataModel) => {
            if(element.labelName == 'tosse') {
                response = (!element.value);
            }
            else if(element.labelName == 'febbre') {
                response = (element.value < 37.5);
            }
        });
        return response;
    }
    
    async submitUserAccess(userId: string, company: any) {
        let response: any;
        const areaId = company.sedi[0].aree[0]._id;
        const companyId = company._id;
        if(areaId != undefined && areaId.toString() != '') {
            (await UserDoc.findOne({_id: userId}).exec().then(async (user : IUser) => {
                if(user == undefined) {
                    return response = createErrorResponseBody(404, 'ERR_USER_NOT_FOUND', 'Searching failed for userId:' + userId);
                }
                let parsingExtService = container.get<ParsingExtService>(SERVICE_TYPES.ParsingExt);
                const lastSnapshot = (await container.get<UserService>(SERVICE_TYPES.User).getLastSnapshot(userId));
                const todayAccesses = (await this.getTodayUserAccess(userId));
                let addressType : EntryType;
                if(todayAccesses.length > 0) {
                    if(this.securityLastEntryDifferentAreaCheck(todayAccesses[todayAccesses.length-1], areaId)) {
                        return response = createErrorResponseBody(400, 'ERR_USER_IN_ANOTHER_AREA', 'Error, the user :' + userId + ' is actually registered in another area');
                    }
                    addressType = this.getAccessType(todayAccesses.filter(access => {
                        return (access.areaId.toString() == areaId);
                    }));
                } else {
                    addressType = EntryType.ENTER;
                }
                if(lastSnapshot == undefined) {
                    return response = createErrorResponseBody(400, 'ERR_NO_SNAPSHOTS_IN_LASTWEEK', 'Error no snaphosts have been found in the last 7 days for user :' + userId);
                }
                const parsingRequest = parsingExtService.getParsingRequestForUserAccess(userId, lastSnapshot._id);
                const parsedData = (await parsingExtService.getDataParsed(parsingRequest));
                let numAvailableSeat : number;
                const entryPermission = (this.checkUserHealthData(parsedData) && (numAvailableSeat = (await this.checkAvailableSeat(companyId))) > 0);
                if(numAvailableSeat == 0 && addressType == EntryType.ENTER) {
                    return response = createErrorResponseBody(400, 'ERR_NO_AVAILABLE_SEAT_AREA', 'Error there is no more seat in the area: ' + areaId);
                }
                let accessTrackerData: IAccessTracker = {
                userId : userId,
                companyId: companyId,
                areaId : areaId,
                timestamp: moment().toDate(),
                type: addressType,
                result: entryPermission
                };
                let newAccess = new AccessTrackerDoc(accessTrackerData);
                await newAccess.save()
                .then(value => {
                    return response = createResponseBody(201, {
                        userId: userId,
                        type: addressType,
                        result: entryPermission,
                        availableSeat: addressType == EntryType.ENTER ? numAvailableSeat-1 : numAvailableSeat+1,
                        timestamp: moment().toDate()
                    }, "Access created for user :" + userId);             
                })
                .catch(error => {
                    return response = createErrorResponseBody(400, 'ERR_ACCESSTRACKER_NOT_CREATED', 'Error during creation of access for user :' + userId);
                });
            }));
       } else {
            return response = createErrorResponseBody(404, 'ERR_COMPANY_NOT_FOUND', 'No companyId has passed for this request');
        }
        return response;
    }

    async getListAccessForCompany(fromDate: string, companyId: string) : Promise<AccessTracker[]>{
        if(companyId == undefined || companyId == '') {
            return;
        }
        let filterConditions = {
            companyId: companyId
        };
        return (await this.getListAccess(fromDate, filterConditions));
    }

    async getListAccessForArea(fromDate: string, areaId: string) : Promise<AccessTracker[]>{
        if(areaId == undefined || areaId == '') {
            return;
        }
        let filterConditions = {
            areaId: areaId
        };
        return (await this.getListAccess(fromDate, filterConditions));
    }

    async getListAccessForSede(fromDate: string, sedeId: string) : Promise<AccessTracker[]>{
        if(sedeId == undefined || sedeId == '') {
            return;
        }
        let filterConditions = {
            sedeId: sedeId
        };
        return (await this.getListAccess(fromDate, filterConditions));
    }

    private async getListAccess(fromDateString: string, idCondition: {}) : Promise<AccessTracker[]>{
        let response: AccessTracker[];
        await AccessTrackerDoc.find(idCondition).exec().then(accesses => {
            if(fromDateString!=undefined) {
                const fromDate = new Date(fromDateString);
                response = accesses.filter(access => {
                    return (moment(access.timestamp).isAfter(moment(fromDate)));
                });
            } else {
                response = accesses;
            }
        });
        return response;
    }
}