import { injectable, id } from "inversify";
import https from "https";
import { connect,disconnect, } from "mongoose";
import config from '../config';
import request from "request";
import { brotliDecompressSync } from "zlib";
import { IParserRequest, ParserRequest, IKeysRequest } from "../models/parserrequest";

const dataExample = {
    templateId:"prova",
    keys: 
    [
        {
        collection:"users",
        keyName:"firstName",
        keyValue:"Ivan"
        },
        {
            collection:"snapshots",
            keyName:"_id",
            keyValue:"5ee24bee8aff9b230cdd4d25"
        }
    ]
}

@injectable()
export class ParsingExtService{
    constructor(){
        
    }

    getParsingRequestForUserAccess(userId: string, snapshotId: string) : ParserRequest {
        let parsingRequestForUserAccess : IParserRequest = {
            templateName: 'submitUserAccessTemplate',
            keys: [
                {
                    collection: 'users',
                    keyName: '_id',
                    keyValue: userId
                },
                {
                    collection: 'snapshots',
                    keyName: '_id',
                    keyValue: snapshotId
                }
            ]
        };
        return parsingRequestForUserAccess;
    }

    async getDataParsed(parserReq: ParserRequest) {
        
        return new Promise<any>((resolve, reject) => {
            request.post(config.parser.url + '/parsing', {
                json : parserReq
                }
                , (error, response, body) => {
                if (error) {
                    console.error(error);
                    return reject(error);
                }
    
                console.log(`Calling data parser service - statusCode: ${response.statusCode}`);
                //console.log(body);
                return resolve(body);
            });
        });
    }
}