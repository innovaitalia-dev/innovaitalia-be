import { injectable, inject } from "inversify";
import { SERVICE_TYPES } from "../di/types";
import { ConversationService } from "./conversation.service";
import { User} from "../models/user";
import { SnapshotDoc } from "../schemas/snapshot";
import {
  ISnapshotData,
  SnapshotDataType,
  ISnaphotDataEntity,
  ISnapshot
} from "../models/snapshot";
import moment = require("moment");
import { UserDocType } from "../schemas/user";

@injectable()
export class UserService {
  constructor(
    @inject(SERVICE_TYPES.Conversation) public convService: ConversationService
  ) {}
  async generateContextForUser(user: UserDocType) {
    let { extraData, result } = await this.analizeSnapshots(user.id,user.lastWeeklyUpdate);
    return {
      personal: this.generateProfileSection(user),
      result: result,
      extra_variables: extraData
    };
  }
  private async analizeSnapshots(userId,lastWeeklyUpdate) {
    let extraData: IExtraVariables; 
    let result: ISnapshotData = {};
    let snapshots = (await SnapshotDoc.find({ userId: userId }).exec()).filter(
      sn => {
        return moment().diff(moment(sn.timestamp), "days") <= 7;
      }
    );
    let todaySnapshot = snapshots.find(sn => {
      return moment(sn.timestamp).isSame(moment().startOf("day"), "d");
    });
    let lastSnapshot:ISnapshot =snapshots[snapshots.length - 1];
    let allowedTypes: Array<SnapshotDataType> = [
      ...(todaySnapshot ? [SnapshotDataType.DAILY] : []),
      ...(snapshots.length > 0 ? [SnapshotDataType.ONETIME] : []),
      ...(moment().diff(moment(lastWeeklyUpdate), "days") < 7
        ? [SnapshotDataType.WEEKLY]
        : [])
        
    ];

    for (const [key, value] of Object.entries(lastSnapshot?lastSnapshot.data:{})) {
      if (allowedTypes.includes((value as ISnaphotDataEntity).type)) {
        result[key] = value as ISnaphotDataEntity;
      }
    }

    extraData = {
      firstTimeEver: snapshots.length > 0 ? false : true,
      firstTimeToday: todaySnapshot ? false : true
    };
    return { extraData, result };
  }
  private generateProfileSection(user: User) {
    console.log("User",user);
    return {
      name: user.firstName,
      cognome: user.lastName,
      indirizzo: user.domicileAddress,
      città: user.domicileCity,
      codiceFiscale: user.personalNumber,
      sesso: user.sex,
      età: user.age
    };
  }

  async getLastSnapshot(userId: string) {
    let snapshots = (await SnapshotDoc.find({ userId: userId }).exec()).filter(
      sn => {
        return moment().diff(moment(sn.timestamp), "days") <= 7;
      }
    );
    return snapshots[snapshots.length-1];
  }
}
interface IExtraVariables {
  firstTimeEver?: boolean;
  firstTimeToday?: boolean;
}
