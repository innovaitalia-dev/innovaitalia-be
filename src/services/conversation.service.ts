import { injectable } from "inversify";
import { Assistant } from "../utils/assistant";
import config from "../config";

@injectable()
export class ConversationService {
  private assistant: Assistant;
  private dialogId: string = "fd6e198a-8c0b-46a1-9e12-586595a8083d";
  constructor() {
    this.assistant = new Assistant({
      credentials: {
        apikey: config.assistant.apiKey,
        url: config.assistant.url
      }
    });
  }
  sessionExists(id) {
    return this.assistant.sessions[id] ? true : false;
  }
  async message(userId, businessJson, text) {
    let res = await this.assistant.message(
      { userId, dialogId: this.dialogId, businessJson: businessJson },
      text
    );

    return {
      context: res.result.context.skills["main skill"].user_defined.data,
      response: res.result.output.generic.map(val => {
        if (val.response_type === "option") {
          return {
            type: ResType.OPTIONS,
            text: val.title,
            options: val.options.map(opt => {
              return {
                label: opt.label,
                value: opt.value.input.text
              };
            })
          };
        } else {
          return {
            type: ResType.TEXT,
            text: val.text
          };
        }
      })
    };
  }
}

enum ResType {
  TEXT = "text",
  OPTIONS = "options"
}
