import { injectable } from "inversify";
import { connect,disconnect, } from "mongoose";
import config from '../config';


@injectable()
export class Db{
    constructor(){

    }
    async init(){
        await connect(config.db.uri, { useNewUrlParser: true ,useUnifiedTopology:true});
    }
    async disconnect(){
        await disconnect();
    }
}