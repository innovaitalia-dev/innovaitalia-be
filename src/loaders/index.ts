import Logger from "./logger";
import expressLoader from "./express";
import container from "../di/inversify.config";
import { Db } from "../services/db.service";
import { SERVICE_TYPES } from "../di/types";

export default async ({ expressApp }) => {

    await container.get<Db>(SERVICE_TYPES.Db).init();
    Logger.info("✌️ Db loaded");

    expressLoader({ app: expressApp });
    Logger.info("✌️ Express loaded");

}
