
export interface IParserRequest {
    templateName: string | any,
    keys: IKeysRequest[] | any,
}

export interface IKeysRequest {
    collection: string | any,
    keyName: string | any,
    keyValue: string | any
}

export class ParserRequest implements IParserRequest {
    templateName: string;
    keys: IKeysRequest[];
}


export interface IParsedDataModel {
    displayName: string | any;
    labelName: string | any;
    value?:  any;
    nodes?: IParsedDataModel[] | any[];
}

export class ParsedDataModel implements IParsedDataModel {
    displayName: string;
    labelName: string;
    value?:  any;
    nodes?: ParsedDataModel[];
}