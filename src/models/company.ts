export interface ICompany {
    nome: string | any,
    codiceFiscale: string | any,
    password: string| any,
    sedi: ISede[] | any,
}

export interface ISede {
    nome: string | any,
    indirizzo: string | any,
    telefono: string | any,
    aree: IArea[] | any,
}

export interface IArea {
    nome: string | any,
    descrizione?: string | any,
    posti: number | any,
    //account: IAccount | any
}

// export interface IAccount {
//     nome: string | any,
//     email: string | any,
//     password: string | any
// }

export class Company implements ICompany {
    nome: string;
    password: string;
    codiceFiscale: string;
    sedi: ISede[];
}