export interface ISnapshot {
  timestamp: Date | any;
  userId: string | any;
  data: ISnapshotData | any;
}
export class Snapshot implements ISnapshot {
  timestamp: Date;
  userId: string;
  data: ISnapshotData;
}
export interface ISnaphotDataEntity {
  value: boolean | number | string;
  type: SnapshotDataType;
  context?:string;
}
export interface ISnapshotData {
  [key: string]: ISnaphotDataEntity;
}



export enum SnapshotDataType {
  ONETIME = "oneTime",
  WEEKLY = "weekly",
  DAILY = "daily"
}
