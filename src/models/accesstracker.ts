
export interface IAccessTracker{
    userId : string | any,
    companyId : string | any,
    areaId : string | any,
    autocertificazione?: IAutocertificazioneFields[] | any,
    timestamp: Date|any,
    type: EntryType | any,
    result: boolean | any
}

export interface IAutocertificazioneFields{
    nome: string | any,
    valore: string | any
}

export class AccessTracker implements IAccessTracker {
    userId : string;
    companyId : string;
    areaId : string;
    autocertificazione?: IAutocertificazioneFields[];
    timestamp: Date;
    type: EntryType;
    result: boolean;
}

export enum EntryType {
  ENTER = "enter",
  EXIT = "exit"
}