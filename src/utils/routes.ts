import { CompanyDocType } from "../schemas/company";
import { UserDocType } from "../schemas/user";

export function createResponse(res, message, body) {

  if(body.errorStatus) {
    createErrorResponse(res, body.errorKey, body.errorMessage, body.errorStatus);
  } else {
    res.json({
      code: res.statusCode,
      body: body,
      message: message
    });
  }
}

export function createErrorResponse(res, errorKey: string, errorMessage: string, statusCode?: number) {

  if(statusCode) {
    res.statusCode = statusCode;
  } else {
    res.statusCode = 404;
  }

  res.json({
    code:res.statusCode,
    errorKey: errorKey,
    errorMessage: errorMessage
  });
}

export function createResponseBody(statusCode: number, body: any, message: string) {
  console.log(message);
  let resp = body;
  resp.code = statusCode;
  resp.message = message;
  return resp;
}

export function createErrorResponseBody(statusCode: number, errorKey: string, errorMessage: string) {
  console.log(errorMessage);
  return {
      code: statusCode,
      errorKey: errorKey,
      errorMessage: errorMessage,
  }
}

export function getAuthorizedCompany(req) : CompanyDocType {
  return req['user'];
}

export function getAuthorizedUser(req) : UserDocType {
  return req['user'];
}