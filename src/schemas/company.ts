import mongoose, { Schema, Document, SchemaDefinition } from "mongoose";
import { Company, IArea, ISede} from "../models/company";

const areaSchema = new Schema({
    nome: String,
    descrizione: String,
    posti: Number,
    // account: {
    //     nome: String,
    //     email: String,
    //     password: String
    // }
})

const sedeSchema = new Schema({
    nome: String,
    indirizzo: String,
    telefono: String,
    aree: [areaSchema]
})
const companySchema = new Schema({
    nome: String,
    password: String,
    codiceFiscale: String,
    sedi: [sedeSchema],
});

export type CompanyDocType = Company & Document;
export type AreaDocType = IArea & Document;
export type SedeDocType = ISede & Document;

export const CompanyDoc = mongoose.model<CompanyDocType>(
    "CompanyDoc", companySchema, "companies"
);

export const SedeDoc = mongoose.model<SedeDocType>("SedeDoc", sedeSchema);

export const AreaDoc = mongoose.model<AreaDocType>("AreaDoc", areaSchema);