import { AccessTracker, EntryType } from "../models/accesstracker";
import mongoose, { Schema, Document } from "mongoose";

const accessTrackerSchema = new Schema({
    userId : {type: Schema.Types.ObjectId, ref: 'UserDoc'},
    companyId : {type: Schema.Types.ObjectId, ref: 'CompanyDoc'},
    areaId : {type: mongoose.Schema.Types.ObjectId, ref: 'CompanyDoc'},
    type: {type: EntryType},
    timestamp: {type: Date},
    result: {type: Boolean}
});
export type AccessTrackerDocType = AccessTracker & Document;

export const AccessTrackerDoc = mongoose.model<AccessTrackerDocType>(
    "AccessTrackerDoc", accessTrackerSchema, "accesstracker"
);